import bot.TelegramBot
import logging.config
import Vk

# Enable logging
logging.config.fileConfig('logging.ini')


def main():
    bot.TelegramBot.init()
    Vk.main()


if __name__ == '__main__':
    main()
