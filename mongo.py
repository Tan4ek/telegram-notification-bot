import pymongo
from configuration import config
import logging

client = pymongo.MongoClient(config['mongodb']['uri'])
logger = logging.getLogger(__name__)

db = client['test1']

#configuration indexs
logger.info('configuration mongodb indexes')