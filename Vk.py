import vk
from configuration import config
from threading import Thread
from dao.FilmsDao import film_dao
from dao.SubscribeDao import subscribe_dao
from dao.VkPostDao import vk_post_dao
import schedule
import time
import logging
import bot.TelegramBot
from telegram.error import BadRequest

# Enable logging
logging.config.fileConfig('logging.ini')

logger = logging.getLogger(__name__)

session = vk.Session(access_token=config['vk']['access-token'])

api = vk.API(session, v='5.69', lang='ru', timeout=10)

def checkPosts():
    posts = api.wall.get(owner_id=config['vk']['group-id'], count=10, extended=1)

    vk_posts = map(
        lambda p: {'post_id': p['id'], 'from_id': p['from_id'], 'owner_id': p['owner_id'], 'date': p['date']},
        posts['items'])

    vk_post_dict = dict((post['id'], post) for post in posts['items'])

    for vk_post in vk_posts:
        if not vk_post_dao.contains_vk_post(vk_post):
            # todo send notification
            post = vk_post_dict[vk_post['post_id']]
            films = film_dao.select_films_by_name(post['text'])
            if len(films):
                for film in films:
                    for subscribe in subscribe_dao.select_enable_by_film_id(film['_id']):
                        try:
                            bot.TelegramBot.telegram_bot.get_bot().send_message(chat_id=subscribe['chat_id'],
                                                                                text='New serials. Post text: ' + post[
                                                                                    'text'])
                        except BadRequest as error:
                            if error.message == 'Chat not found':
                                subscribe_dao.disable_subscribe(subscribe, error)
                            logger.error('Send message to chat_id "%s" error. "%s"', subscribe['chat_id'], error)
                        except Exception as error:
                            logger.error('Send message to chat_id "%s" unchecked error "$s"', subscribe['chat_id'],
                                         error)

            logger.info('New post: ' + str(vk_post))
            vk_post_dao.save_vk_post(vk_post)
        else:
            print('Old post' + str(vk_post['post_id']))


def main():
    def job():
        try:
            checkPosts()
        except Exception as e:
            logger.error('Check posts exception.', exc_info=True)

    schedule.every(10).seconds.do(job)

    def threaded_function():
        while True:
            schedule.run_pending()
            time.sleep(1)

    thread = Thread(target=threaded_function)
    thread.start()


if __name__ == '__main__':
    main()
