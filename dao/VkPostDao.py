from mongo import db
import pymongo


class VkPostDao:

    def __init__(self, mongo):
        if 'vk_posts' not in mongo.collection_names():
            validator = {
                '$and': [
                    {'post_id': {'$exists': True}},
                    {'owner_id': {'$exists': True}},
                    {'date': {'$exists': True}}
                ]
            }
            self.db = mongo.create_collection('vk_posts', validator=validator)
            db.vk_posts.create_index([('date', pymongo.ASCENDING)], unique=True)
            db.vk_posts.create_index([('post_id', pymongo.ASCENDING)], unique=True)
            db.vk_posts.create_index([('owner_id', pymongo.ASCENDING)], unique=True)
            db.vk_posts.create_index([('from_id', pymongo.ASCENDING)], unique=True)
        else:
            self.db = mongo.vk_posts

    def save_vk_post(self, vk_post):
        self.db.save(vk_post)

    def contains_vk_post(self, vk_post):
        return self.db.find_one(
            {'post_id': vk_post['post_id'], 'owner_id': vk_post['owner_id'], 'from_id': vk_post['from_id']}) is not None


vk_post_dao = VkPostDao(db)
