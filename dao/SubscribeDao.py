from mongo import db
import pymongo
from datetime import datetime


class SubscribeDao:

    def __init__(self, mongo):
        if 'subscribe' not in mongo.collection_names():
            validator = {
                '$and': [
                    {'user_id': {'$exists': True}},
                    {'chat_id': {'$exists': True}}
                ]
            }
            self.db = mongo.create_collection('subscribe', validator=validator)
        else:
            self.db = mongo.subscribe
        self.db.create_index(
            [('user_id', pymongo.ASCENDING), ('chat_id', pymongo.ASCENDING), ('subscribes.film_id', pymongo.ASCENDING),
             ('enable', pymongo.ASCENDING)],
            unique=True)

    def save_subscribe(self, subscribe):
        self.db.save(subscribe)

    def select_by_chat_id(self, chat_id):
        return self.db.find_one({'chat_id': chat_id})

    def select_enable_by_film_id(self, film_id):
        return list(self.db.find({'subscribes.film_id': film_id, 'enable': True}))

    def disable_subscribe(self, subscribe, reason_error):
        subscribe['enable'] = False
        subscribe['error'] = {'message': reason_error.message, 'errorClass': reason_error.__class__.__name__,
                              'time': datetime.now()}
        self.db.save(subscribe)


subscribe_dao = SubscribeDao(db)
