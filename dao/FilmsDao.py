from mongo import db
import pymongo


class FilmsDao:

    def __init__(self, mongo):
        if 'films' not in mongo.collection_names():
            validator = {
                '$and': [
                    {'name': {'$exists': True, '$type': 'string'}}
                ]
            }
            self.db = mongo.create_collection('films', validator=validator)
        else:
            self.db = mongo.films
        self.db.create_index([('name', pymongo.TEXT)])

    def save_film(self, film):
        if film['name'] is None or not film['name']:
            raise ValueError('film name not exist')
        self.db.insert_one(film)

    def select_films(self):
        return list(self.db.find())

    def select_films_by_name(self, name):
        return list(self.db.find({'name': name}))


film_dao = FilmsDao(db)
