#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.

This program is dedicated to the public domain under the CC0 license.

This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

# Test chat id - 353209590

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
from configuration import config
from dao.FilmsDao import film_dao
from dao.SubscribeDao import subscribe_dao
import telegram.bot
from telegram.ext import messagequeue as mq

logger = logging.getLogger(__name__)


class MQBot(telegram.bot.Bot):
    '''A subclass of Bot which delegates send method handling to MQ'''

    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(MQBot, self).__init__(*args, **kwargs)
        # below 2 attributes should be provided for decorator usage
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = mqueue or mq.MessageQueue()

    def __del__(self):
        try:
            self._msg_queue.stop()
        except:
            pass
        super(MQBot, self).__del__()

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        '''Wrapped method would accept new `queued` and `isgroup`
        OPTIONAL arguments'''
        super(MQBot, self).send_message(*args, **kwargs)


class TelegramBot:
    def __init__(self):
        """Start the bot."""
        # Create the EventHandler and pass it your bot's token.
        queue = mq.MessageQueue(all_burst_limit=29, all_time_limit_ms=1017)
        bot = MQBot(config['telegram-bot']['token'], mqueue=queue)

        updater = Updater(bot=bot)

        # Get the dispatcher to register handlers
        dp = updater.dispatcher

        self.__bot = bot

        # on different commands - answer in Telegram
        dp.add_handler(CommandHandler("start", self.__start))
        dp.add_handler(CommandHandler("help", self.__help))
        dp.add_handler(CommandHandler("subscribe", self.__subscribe, pass_args=True))
        dp.add_handler(CommandHandler("unsubscribe", self.__unsubscribe, pass_args=True))
        dp.add_handler(CommandHandler("serials", self.__serials))
        dp.add_handler(CommandHandler("unsubscribe_all", self.__unsubscribe_all))

        # on noncommand i.e message - echo the message on Telegram
        dp.add_handler(MessageHandler(Filters.text, self.__echo))

        # log all errors
        dp.add_error_handler(self.__error)

        # Start the Bot
        updater.start_polling()

    # Define a few command handlers. These usually take the two arguments bot and
    # update. Error handlers also receive the raised TelegramError object in error.
    def __start(self, bot, update):
        """Send a message when the command /start is issued."""
        update.message.reply_text('Hi!')

    def __help(self, bot, update):
        """Send a message when the command /help is issued."""
        update.message.reply_text('Help!')

    def __echo(self, bot, update):
        """Echo the user message."""
        update.message.reply_text(update.message.text)

    def __subscribe(self, bot, update, args):
        """Subscribe to series"""
        # return names of serials
        film_name = args[0]
        films = film_dao.select_films_by_name(film_name)
        if len(films) > 1:
            for film in films:
                update.message.reply_text(film['name'])
        elif len(films) == 0:
            update.message.reply_text('film not found: ' + film_name)
        else:
            film = films[0]
            chat_id = self.__get_chat_id(update)
            subscribe_entity = subscribe_dao.select_by_chat_id(chat_id)
            if subscribe_entity is None:
                subscribe_dao.save_subscribe({
                    'chat_id': chat_id,
                    'user_id': update.message.from_user.id,
                    'user_first_name': update.message.from_user.first_name,
                    'user_lang_code': update.message.from_user.language_code,
                    'user_name': update.message.from_user.name,
                    'enable': True,
                    'user_username': update.message.from_user.username,
                    'subscribes': [{'film_id': film['_id']}]
                })
                update.message.reply_text('You subscribe to the film: ' + film['name'])
            else:
                subscribe_film = next((s for s in subscribe_entity['subscribes'] if s['film_id'] == film['_id']), None)
                if subscribe_film is None:
                    subscribe_entity['subscribes'] = list(subscribe_entity['subscribes'])
                    subscribe_entity['subscribes'].append({'film_id': film['_id']})
                    subscribe_dao.save_subscribe(subscribe_entity)
                else:
                    update.message.reply_text('You already subscribe to: ' + film['name'])

    def __get_chat_id(self, update):
        return update.message.chat_id

    def __unsubscribe(self, bot, update, args):
        """Unsubscribe to series"""
        # return names of serials
        if len(args) == 0 or args[0] is None:
            update.message.reply_text('Input film name as first argument: /unsubscribe <film_name>')
            return
        films = film_dao.select_films_by_name(args[0])
        if len(films) == 0:
            update.message.reply_text("Film not found")
            return
        elif len(films) > 1:
            # todo one message
            update.message.reply_text('Find multiply films: ')
            for film in films:
                update.message.reply_text(film['name'])
        subscribe_entity = subscribe_dao.select_by_chat_id(self.__get_chat_id(update))
        film = films[0]
        if subscribe_entity is None:
            update.message.reply_text('You doesn\'t subscribe to any film')
        else:
            subscribes = list(filter(lambda s: s['film_id'] != film['_id'], subscribe_entity['subscribes']))
            if len(subscribes) == len(subscribe_entity['subscribes']):
                update.message.reply_text('You doen\'t subscribe to the film')
            else:
                subscribe_entity['subscribes'] = subscribes
                subscribe_dao.save_subscribe(subscribe_entity)
                update.message.reply_text('You has been success unsubscribed')

    def __unsubscribe_all(self, bot, update):
        subscribe_entity = subscribe_dao.select_by_chat_id(self.__get_chat_id(update))
        subscribe_entity['subscribes'] = list()
        subscribe_dao.save_subscribe(subscribe_entity)
        update.message.reply_text('You has been success unsubscribed of all messages')

    def __serials(self, bot, update):
        # """Return list of serials"""
        update.message.reply_text("Available serials:")
        films = film_dao.select_films()
        for film in films:
            update.message.reply_text(film['name'])

    def __error(self, bot, update, error):
        """Log Errors caused by Updates."""
        logger.error('Update "%s" caused error "%s"', update, error)

    def get_bot(self):
        return self.__bot


telegram_bot = TelegramBot()


def init():
    print("Test")


# temporary solution. I'm sorry
if __name__ == "__main__":
    init()
